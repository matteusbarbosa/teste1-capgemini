$(document).ready(function(){

    $('.btn-search').on('click', function(){
        $('#form-search').submit();
    });

    $('.btn-toggle').on('click', function(e){
        e.preventDefault();
        $('.input-search input').focus();
    });
$('.input-search input').on('focusin', function(){
    $(this).siblings('.btn-toggle').css({'display' : 'none'});
    $(this).siblings('.btn:not(.btn-toggle)').css({'display' : 'inline-block'});
});

$('.input-search input').on('focusout', function(e){
    if($(e.relatedTarget).hasClass('btn-search') == false){
        $(this).siblings('.btn-toggle').css({'display' : 'inline-block'});
        $(this).siblings('.btn:not(.btn-toggle)').css({'display' : 'none'});
    }
});

});