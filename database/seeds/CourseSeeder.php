<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $number_consultants = 3;
        $number_courses = 10;
        $number_locations = 10;
        $faker = Faker\Factory::create();

        for($cst = 0; $cst < $number_consultants; $cst++){

            $consultant_id = DB::table('consultant')->insertGetId([
                'avatar' =>  $faker->imageUrl(),
                'name' =>   $faker->name(),
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);

            for($crs = 0; $crs < $number_courses; $crs++){

                $course_id = DB::table('course')->insertGetId([
                    'title' =>  $faker->name,
                    'category' =>  $faker->jobTitle,
                    'description' => $faker->realText($maxNbChars = 200, $indexSize = 2),
                    'price' => $faker->randomDigit,
                    'start' => $faker->dateTimeBetween($startDate = '-30 years', $endDate = 'now', $timezone = null),
                    'finish' => $faker->dateTimeBetween('now', $endDate = '+1day', $timezone = null),
                    'created_at' => $faker->dateTime(),
                    'updated_at' => $faker->dateTime(),
                ]);


                for($lct = 0; $lct < $number_locations; $lct++){



            $location_id = DB::table('location')->insertGetId([
                'street' =>  $faker->streetName,
                'number' =>  $faker->randomDigitNotNull,
                'neighborhood' => $faker->secondaryAddress,
                'city' => $faker->city,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);

            $meeting_id = DB::table('meeting')->insertGetId([
                'course_id' =>  $course_id,
                'consultant_id' =>  $consultant_id,
                'location_id' => $location_id,
                'created_at' => $faker->dateTime(),
                'updated_at' => $faker->dateTime(),
            ]);
        }
    }
}

    }
}
