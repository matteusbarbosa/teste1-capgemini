@extends('layouts.app')

@section('title', 'Pagamentos')

@section('header')
@parent
@endsection

@section('content')
<div class="container container-meetings">
<div class="row">
    <div class="col-md-10 offset-md-1">
        <div class="row">


    @foreach($data['meetings'] as $k => $meeting)

    <div class="col-md-4 col-card   mb-3">
        <a href="{{ url('/meetings/'.$meeting->id) }}">
<div class="card card-meeting border-secondary m-auto" style="max-width: 20rem;">
                <div class="card-header">{{ $meeting->category }}</div>
                <div class="card-body">
                  <h4 class="card-title">{{ $meeting->title }}</h4>
                </div>
                <div class="card-footer">
                        {{ date('j \d\e F \d\e Y', strtotime($meeting->start)) }}
                        <i class="fa fa-calendar float-right"></i>
                </div>
              </div>
            </a>
              </div>
              @endforeach
              </div>
            </div>
            </div>
            

</div>

@endsection