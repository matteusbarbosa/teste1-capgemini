<div class="bs-component">
              <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="{{ url('/meetings') }}">{{ trans('legend.courses') }}</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
                  <i class="fa fa-search"></i>
                </button>

                <div class="collapse navbar-collapse" id="navbarColor01">
           
                  <form id="form-search" class="form-inline m-auto" action="{{ url('/meetings/search') }}" method="GET">
                    @csrf
                    <div class="input-search m-auto">
                  <button class="btn btn-link btn-toggle" type="button"><i class="fa fa-search" ></i></button>
                    <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar" name="term">
                    <button class="btn btn-link btn-search" type="submit"><i class="fa fa-search"></i></button>
</div>
                  </form>
                </div>
              </nav>
            </div>