<div class="container mt-3">
<div class="row">
    <div class="col-md-8 offset-2 mb-3">
@if(session('alert-success'))
<div class="alert alert-success" role="alert">
{{ trans('notification.'.session('alert-success')) }}
</div>
@endif
</div>
</div>
</div>