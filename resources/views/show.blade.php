@extends('layouts.app')

@section('title', trans('legend.course'))

@section('header')
@parent
@endsection

@section('content')
<div class="container mt-3">
<div class="row">
    @if(isset($data['meeting']))
    <div class="col-md-8 offset-md-2 card-show  mb-3">
                <div class="card-body">
                  <h4 class="card-title">{{ $data['meeting']->course->title }}</h4>
                  <p>{{ $data['meeting']->course->description }}</p>
                  <table class="table table-meeting">
                      <tbody>
                          <tr>
                              <th><i class="fa fa-calendar-minus-o"></i></th>
                              <td>{{ date('j \d\e F \d\e Y', strtotime($data['meeting']->course->start)) }}</td>
                          </tr>
                          <tr>
                              <th><i class="fa fa-clock-o"></i></th>
                              <td>De {{ date('H:i', strtotime($data['meeting']->start)) }} às {{ date('H:i\h', strtotime($data['meeting']->finish)) }} - {{ $data['meeting']->length }}h</td>
                          </tr>
                          <tr>
                              <th><i class="fa fa-map-marker"></i></th>
                              <td><a href="">{{ $data['meeting']->location->street.', '.$data['meeting']->location->number }}</a></td>
                          </tr>
                          <tr>
                              <th><i class="fa fa-dollar"></i></th>
                              <td>{{ $data['meeting']->course->price }}</td>
                          </tr>
                          <tr>
                              <th><i class="fa fa-tag"></i></th>
                              <td>{{ $data['meeting']->course->category }}</td>
                          </tr>

                           <tr>
                              <th><div class="img-avatar-wrap"><figure><img src="{{ $data['meeting']->consultant->avatar }}" class="img-fluid"></figure></div></th>
                              <td><p  class="align-middle">{{ $data['meeting']->consultant->name }}</p></td>
                          </tr>
                      </tbody>
                  </table>
                  <div class="text-center">
@if(!$data['meeting']->subscribed)
    <form method="POST" action="{{ url('/subscription') }}" >
        @csrf
        <input type="hidden" name="meeting_id" value="{{ $data['meeting']->id }}">
        <input type="hidden" name="user_id" value="1">
        <button type="submit" class="btn-subscribe btn ">{{ trans('legend.subscribe') }}</button>
    </form>
    @else
    <div class="alert alert-info" role="alert">
{{ trans('notification.subscription-sent') }}
</div>
    @endif
</div>
                  
                </div>
              </div>
              @endif
              </div>
</div>

@endsection