'use strict';

var gulp = require('gulp');

var sass = require('gulp-sass');

var gutil = require('gulp-util');

var ftp = require('gulp-ftp');

var sourcemaps = require('gulp-sourcemaps');

const errorHandler = require('gulp-error-handle');

const logError = function(err) {
  gutil.log(err);
  this.emit('end');
};

gulp.task('sass_style', function () {
  return gulp.src('public/css/sass/style.scss')
  .pipe(sass({outputStyle: 'compressed'})
  .on('error', sass.logError))
  .pipe(sourcemaps.init())
  .pipe(sourcemaps.write('maps'))
  .pipe(gulp.dest('public/css'));
});



gulp.task('watch', function () {
  gulp.watch('public/css/sass/style.scss', ['sass_style']);
});

gulp.task('default', [ 'watch' ]);
