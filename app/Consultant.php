<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consultant extends Model
{
    protected $table = 'consultant';

    public function meetings(){
        return $this->hasMany('App\Meeting');
    }
}
