<?php

namespace App;

use \App\Course;
use \App\Consultant;
use \App\Location;
use Illuminate\Support\Facades\DB;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $table = 'meeting';

    public function course(){
        return $this->belongsTo('App\Course');
    }
        public function consultant(){
            return $this->belongsTo('App\Consultant');
        }
            public function location(){
                return $this->belongsTo('App\Location');
            }

            public static function list($term = null){

                $columns = DB::getSchemaBuilder()->getColumnListing('course');
        
        $query = Course::select('*');
        
        foreach($columns as $column)
        {
          $query->orWhere('course.'.$column, 'LIKE', '%'.$term.'%');
        }

        $query->join('meeting', 'course.id' , '=', 'meeting.course_id');
        $query->inRandomOrder();
        
        $models = $query->get();
        
        return $models;
        
        
            }
}
