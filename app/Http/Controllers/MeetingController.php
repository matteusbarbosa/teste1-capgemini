<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Meeting;
use \App\Course;
use \App\Subscription;

class MeetingController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $data = [];
        $data['meetings'] = Meeting::list();
        return view('list')->with('data', $data);
    }
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function search(Request $request)
    {           
        $data = [];
        $data['meetings'] = Meeting::list($request->query('term'));
        
        return view('list')->with('data', $data);
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        //
    }


    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $data = [];
        $data['meeting'] = Meeting::find($id);

        //$user_id = User::find(Auth::user()->id);
        $user_id = 1;
    
        $data['meeting']->length = Course::getCourseLength($data['meeting']->course->start, $data['meeting']->course->finish);
        $data['meeting']->subscribed = Subscription::where('user_id', $user_id)->where('meeting_id', $id)->first();
        
        return view('show')->with('data', $data);
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        //
    }
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        //
    }
}
