<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $table = 'subscription';

    public function meeting(){
        return $this->belongsTo('App\Meeting');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
