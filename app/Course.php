<?php

namespace App;

use \App\Meeting;

use Illuminate\Database\Eloquent\Model;


class Course extends Model
{
    protected $table = 'course';

    public function meetings(){
        return $this->hasMany('App\Meeting');
    }

    public static function getCourseLength($start, $finish){
        $date_start = strtotime($start);
        $date_finish = strtotime($finish);
        $length_hours = (int) (($date_finish - $date_start) / 3600);
        $length_mins = (int) (($date_finish - $date_start) % 3600 / 60);
        
        return $length_hours.':'.$length_mins;
    }
}
