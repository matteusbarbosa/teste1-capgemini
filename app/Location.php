<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $table = 'location';

    public function meetings(){
        return $this->hasMany('App\Meeting');
    }
}
